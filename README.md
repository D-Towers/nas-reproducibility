# NAS Reproducability

This repo is designed to host docker containers for NAS (Neural Architecture Search) algorithms, to lengthen reproducibility. The idea is to streamline the process for running these algorithms.

After satisfying the requirements, you should be able to simply download the dockerfiles, build them and run them. All the requirements should have been dealt with and interaction with the code, such as supplying data should now be somewhat standardised amongst these containers.

## Requirements
### Docker
These containers are built using docker. Please follow the instructions on the [install docker engine page](https://docs.docker.com/engine/install/) to install docker.

*(Note. Podman and libpod have not been tested but may work)*

### NVIDIA-container-toolkit
As these containers are used for deep-learning, they require access to the GPU. Docker by default cannot do this, so for this we reccomend installing [nvidia-container-toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker).

*(Note. nvidia-docker2 has not been tested, but should work, with the same commands)*

### GPU
As mentioned above, these containers require a GPU to run. The drivers for the GPU also need to be installed on the main system, however, cudnn and cudatoolkit will be installed inside the containers.

Another important note, is most of these containers are using cudatoolkit <= 10. RTX 30 series cards and above may not be compatible with older toolkits. 

## Authors and acknowledgment
[David Towers](https://gitlab.com/D-Towers/nas-reproducibility/-/blob/1-add-enas-container/gitlab.com/D-Towers) is the primary author of this repo.

As this repo uses various different peoples work, they are cited in the README.md in the sections where their work is used.

Changes to the Authors code, and instructions on how we reproduced these algorithms outside of docker can be found in the `reproducing NAS environments.md` file.

## Citation
If you use any these containers in your research, please cite our repo.
