def get_env_var(env, opt1, opt2):
    import os
    """Retrieves the environment variables from the docker container as strings.
    
    Args:
        env (str): The name of the environent variables.
        opt1 (str): The first of two options the environment variable is allowed to be.
        opt2 (str): The second of two options the environment variable is allowed to be.
    
    Returns:
        str: The value of env converted to a str. (Note. this will be equak to either opt1 or opt2).
        
    Raises:
        AttributeError: If env is not either opt1 or opt2. This will results in program termination.
    """
    try:
        var = str(os.getenv(env)).lower()
        if var != opt1 and var != opt2:
            raise AttributeError
    except AttributeError:
        print(var, 'is not a recognised option for', env, ', the valid options are \'', opt1, '\' and \'', opt2, '\'\n')
        exit(1)
    return var