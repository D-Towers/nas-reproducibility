from setuptools import setup

setup(
    name='launcherutils',
    version='1.0.0',
    author='David Towers',
    packages=['launcherutils']
)