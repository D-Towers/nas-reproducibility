import json
import os
import shutil
import distutils.util
from launcherutils import utils

def main():
    """Retrieves the environment vairbales, then runs the corresponding ENAS scripts
    
    Prepares the configuration to run ENAS (https://github.com/melodyguan/enas) in a docker container with a prebuilt environment. Then 
    runs the configuration until completion and then moves the files back out of the docker container, important that the correct file is mounted.
    Changes to the official codes includes a bug fix and the avoidance of bash scripts to set up the configuration. 
    """
    print('Preparing to Launch ENAS ' + STAGE + ' phase using the ' + SEARCH_SPACE + ' search space!')
    #print(get_parameter_string())
    os.system('export PYTHONPATH="/enas/"; python /enas/src/cifar10/main.py ' + get_parameter_string())
  
    files = os.listdir(PATH)
    for file in files:
        shutil.move(os.path.join(PATH, file), os.path.join('/enas/dockout', file))

def get_defined_parameters(param_dict):
    """Retrieves the parameters that are guaranteed
    
    Unlike get_undefined_parameters() and get_custom_parameters() these parameters are easily inferred from the run type, and don't require a 
    JSON object to build. So these parameters are directly added/overwritten (if someone tried to update them in the custom.json) to ensure they
    are correct.
    
    Args:
        param_dict (dict): A dict already containing some of the parameters for ENAS to run.
    
    Returns:
        dict: keys are the parameter names, and the values are the parameter values.
    """
    ss_param = 'search_for'
    dp_param = 'data_path'
    od_param = 'output_dir'
    
    if ss_param in param_dict:
        print("[Warning] Please do not put 'search_for' in custom.json, this is set from the environment variables when the container is run.")
    param_dict[ss_param] = SEARCH_SPACE
    if dp_param in param_dict:
        print("[Warning] Please do not put 'data_path' in custom.json, docker moves your data into the correct place for you.")
    param_dict[dp_param] = 'data/cifar10'
    if od_param in param_dict:
        print("[Warning] Please do not put 'output_dir' in custom.json, this is predefined so docker knows how to retrieve your output.")
    param_dict[od_param] = OUTPUT_DIR
    return param_dict

def get_undefined_parameters():
    """Retrieves the parameters for ENAS from settings.json
    
    This function retrieves the default parameters for the run type from settings.json and puts them into a dict.
    
    Returns:
        dict: A dictionary of the ENAS parameters. Keys are parameters, values are the corresponding parameter values.
    """
    with open(os.path.join(os.path.dirname(__file__), SETTINGS_FILE_PATH), 'r') as settings_file:
        settings_json = json.load(settings_file)
        run_parameters = settings_json[get_run_type()]
        settings_dict = dict()
        for param in run_parameters:
            value = run_parameters[param]
            settings_dict[param] = value
    return settings_dict

def get_custom_settings(curr_dict):
    """Retrieves the parameters for ENAS from custom.json
    
    This function retrieves the custom parameters from custom.json and updates the given dict to reflect the updated/additional parameters.

    Args:
        curr_dict (dict): The parameter dict that is being updated/overwritten with custom values.

    Returns:
        dict: An updated dict which contains the previous parameters, with updated/additional parameters retrieved from the custom.json file.
    """
    with open(os.path.join(os.path.dirname(__file__), CUSTOM_FILE_PATH), 'r') as custom_file:
        custom_settings = json.load(custom_file)
        for param in custom_settings:
            curr_dict[param] = custom_settings[param]
    return curr_dict

def get_parameter_string():
    """Generates the parameter string appended to the ENAS call, to give it the correct settings.py
    
    Builds the parameter string for the ENAS call to properly run, the dict is first built by calling the default settigns from get_undefined_parameters(),
    then if USE_CUSTOM == True, any custom settings will be appended/used to update the dict. Finally, get_defined_parameters() is called, to make sure these
    values are not overwritten. When building the string, parameters that don't require values, are given the value True, which is detected as a bool so the 
    parser doesn't then add a parameter.

    Returns:
        str: A string formatted so the shell understands them as parameters for the python file.
    """
    settings_dict = get_undefined_parameters()
    if USE_CUSTOM:
        settings_dict = get_custom_settings(settings_dict)
    settings_dict = get_defined_parameters(settings_dict)
    param_string = ''
    for k, v in settings_dict.items():
        param_string += '--' + k
        if type(v) != bool:
            # If the value is a string put quotes around it, otherwise return it as a str
            # in python3+ this would be a check if it is a string, but 2.7 has a seperate unicode type, and my IDE has a warning as it doesn't understand it.
            param_string += '=' + (str(v) if type(v) == float or type(v) == int else quote_value(v))
        param_string += ' \\\n'
    param_string += '"$@"'
    return param_string

def quote_value(value):
    """Surronds the given value in quotes, as bash requires strings to be in quotes.
        
    Examples:
        >>> print(quote_value('Example'))
        "Example"
    """
    return '"' + value + '"'

def get_run_type():
    """Returns a string giving the run configuration.py, used to get parameters from JSON
    
    Returns:
        str: the run configuration of the current run. (Example return: micro search)
    """
    return SEARCH_SPACE + ' ' + STAGE

OUTPUT_DIR = 'output'
CUSTOM_FILE_PATH = 'custom.json'
SETTINGS_FILE_PATH = 'settings.json'
PATH = '/enas/' + OUTPUT_DIR + '/'
STAGE = utils.get_env_var('STAGE', 'search', 'evaluation')
SEARCH_SPACE = utils.get_env_var('SEARCH_SPACE', 'micro', 'macro')
USE_CUSTOM = distutils.util.strtobool(utils.get_env_var('USE_CUSTOM', 'true', 'false'))

if __name__ == "__main__":
  main()
