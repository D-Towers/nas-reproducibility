# Usage
## Building
Once NVIDIA-container-toolkit (or equivelents) is installed, you can build the container using:
<br> ```docker build -t enas .```

## Running
Before running the contianer, you need to create the folder the container will mount, for the given command below, we use `dockout` in the enas directory container the Dockerfile. *(i.e. run `mkdir ./dockout` in the directory with the Dockerfile)*.

To run the container, use:
<br> ```docker run --gpus all --rm -e STAGE=<stage> -e SEARCH_SPACE=<search_space> --mount type=bind,source="$(pwd)/dockout",target="/enas/dockout" enas```

If you want to use the terminal, or want to disconnect from an ssh session, we reccomend using screen or nohup.

### Searching and Evaluating
To-Do

#### Searching
To-Do

#### Evaluating
To-Do

### Environment Variables
To allow you to choose the search space and stage of ENAS you wish to run, we use environment variables inside the docker container to take in input.

STAGE and SEARCH_SPACE are required parameters, an AttributeError will be raised if they are missing. USE_CUSTOM will default to false is missing. (USE_CUSTOM is required along with a modified custom.json in order to evaluate searched architectures (see below))

| Variable          | Option 1      | Option 2      |
|-------------------|---------------|---------------|
| **STAGE**         | search        | evaluation    |
| **SEARCH_SPACE**  | micro         | macro         |
| **USE_CUSTOM**\*  | true          | false         |

*(i.e. STAGE=search and SEARCH_SPACE=micro will run the search phase on the micro search space)*

## Custom Parameters
The docker container is designed so it will run without any additional parameters, however, as you will probably want to evaluate a searched for architecture, or you may want to experiment with different parameters, you can modify the custom.json file to contain custom parameters which will overwrite the default.

**To use these custom parameters, you must add '-e USE_CUSTOM=True' to the docker run command shown above**

### Usable Parameters
Below is a table of the parameters you can add to the custom.json file:

| Parameter Name                    | Note                                  |
|-----------------------------------|---------------------------------------|
|child_fixed_arc                    | This is for setting the arhictecture. |
|eval_every_epochs                  | Defaults to 1.                        |
|nocontroller_training              | To use, give the value true in JSON.  |
|controller_op_tanh_reduce          |                                       |
|log_every                          | Defaults to 50.                       |
|child_num_cell_layers              |                                       |
|num_epochs                         |                                       |
|controller_tanh_constant           |                                       |
|child_lr_cosine                    | To use, give the value true in JSON.  |
|controller_train_every             |                                       |
|child_num_cells                    |                                       |
|child_use_aux_heads                | To use, give the value true in JSON.  |
|controller_search_whole_channels   | To use, give the value true in JSON.  |
|controller_skip_target             |                                       |
|controller_num_aggregate           |                                       |
|child_lr_min                       |                                       |
|controller_lr                      |                                       |
|controller_train_steps             |                                       |
|child_num_layers                   |                                       |
|child_keep_prob                    |                                       |
|child_lr_max                       |                                       |
|batch_size                         |                                       |
|child_lr_T_mul                     |                                       |
|child_l2_reg                       |                                       |
|controller_skip_weight             |                                       |
|controller_entropy_weight          |                                       |
|child_num_branches                 |                                       |
|reset_output_dir                   | To use, give the value true in JSON.  |
|child_out_filters                  |                                       |
|controller_sync_replicas           | To use, give the value true in JSON.  |
|data_format                        | Defaults to 'NCHW'                    |
|child_lr_T_0                       |                                       |
|child_drop_path_keep_prob          |                                       |
|controller_training                | To use, give the value true in JSON.  |

For more infomation on any of the parameters, please check with the official ENAS github (linked below in Acknowledgements) or ask the original authors.

### Example
Below is an example custom.json containing parameters with a custom values and a parameter that doesn't need a value (true is given as the value or these parameters to tell the parse to give it no value when building the parameter string)

```JSON
{
    "child_fixed_arc": "1 1 0 3 2 1 1 0 1 4 1 2 1 3 0 0 0 3 0 1 0 0 0 1 0 4 1 3 0 0 1 0 1 2 0 3 0 1 1 2",
    "num_epochs": 12,
    "controller_training": true
}
```

# Authors and Acknowledgement
This repo is managed and maintained by [David Towers](gitlab.com/D-Towers).

ENAS, and the original code, is by Pham et al.: <br/>
&nbsp;&nbsp; Paper: https://arxiv.org/abs/1802.03268 <br/>
&nbsp;&nbsp; Github: https://github.com/melodyguan/enas

# License
This project includes files from NVIDIA Cudnn 7.0.5 for cudatoolkit 9.0. These are being distributed under NVIDIA's cudnn [software license agreement](https://docs.nvidia.com/deeplearning/cudnn/sla/index.html) section 8. If anyone from NVIDIA has any problems with any distribution we are doing, please get in contact.
