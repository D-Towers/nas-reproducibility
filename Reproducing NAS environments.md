# Reproducing NAS environments
This file contains the changes made to the NAS approaches that we have reproduced in order to get the environment working.

These isntructions are for recreating the environments in conda.

## List of Contents
- ENAS
- DARTS
- PC-DARTS
- DrNAS

## ENAS
1. conda create -n <your env name> python=2.7
2. pip install tensorflow==1.7 tensorflow-gpu==1.7 tensorboard==1.7
3. conda install cudatoolkit=9.0
4. Download CuDNN 7.05 for 9.0 from NVIDIA (you'll need a developer account) and install as:
```bash
sudo cp cuda/include/cudnn*.h ~/anaconda3/envs/<your env name>/include
sudo cp cuda/lib64/libcudnn*.h ~/anaconda3/envs/<your env name>/lib
```
5. Get the dataset batches
6. To use GPU change any instance of `cpu:0` to `gpu:0` in thses files:
    - ./src/cifar10/
        - general_child.py
        - micro_child.py
        - models.py
7. Search with:
`CUDA_VISIBLE_DEVICES=0 nohup sh scripts/cifar10_micro_search.sh > search_micro.log 2>&1 &`
8. Train and evaluate with:
`CUDA_VISIBLE_DEVICES=0 nohup sh scripts/cifar10_micro_finish.sh > search_micro.log 2>&1 &`

### Macro search
In addition to steps 1-6 above to run macro search you need to:
1. Change this line in./src/cifar10/general_child.py:
	-  `out = tf.case(branches, default=lambda: tf.constant(0, tf.float32), exclusive=True)`
	to
	- `out = tf.case(branches, default=lambda: tf.constant(0, tf.float32, shape=[self.batch_size, out_filters, inp_h, inp_w]), exclusive=True)` 
	found in [here](https://github.com/melodyguan/enas/issues/4)
2. To run the eval phase with a custom arch you need to fix another bug in `./src/cifar10/general_child.py` this solution can be found [here](https://github.com/melodyguan/enas/issues/41).
3. run with `CUDA_VISIBLE_DEVICES=0 nohup sh scripts/cifar10_macro_search.sh > search_macro.log 2>&1 &`
4. Once complete run `CUDA_VISIBLE_DEVICES=0 nohup sh scripts/cifar10_macro_finish.sh > finish_macro.log 2>&1 &` 

## DARTS
The given dependency of PyTorch 0.3.1 is no longer available on linux, to convert DARTS to more up to date versions in these file `train.py`, `train_search.py`, `test.py`, `utils.py` & `architect.py`:

1. Change any instance of `cuda(async=True)` to `cuda()`
2. Change any instance of `.data[0]` to `.item()`
3. Change line 35 in 'utils.py' from `.view(-1)` to `.reshape(-1)`
4. Changed `sub` and `add` calls so the params where the otehr way round. architect.py (also put `alpha=` infront of the (now) second param)
5. converted `get_lr()[0]` to `get_last_lr()[0]`

### Searching and Training
1. Then search in the `cnn` file with: `nohup python train_search.py > search.log 2>&1 &`
2. Take the Genotype of the latest epoch from the log file and add it to `genotypes.py` with a name (i.e. `newgeno = Genotype(...)`)
3. Train the search architecture fully with: `nohup python train.py --auxiliary --arch <name of genotype (i.e DARTS or newgeno)> > train.log 2>&1 &`
4. Test the final model with: `nohup python test.py --auxiliary --model_path <path to model> > test.log 2>&1 &`

## PC-DARTS
1. Swap the files in the `V100_python1.0` (train.py and train_search.py) for the files in the main folder.
2. In `test.py` replace `cuda(async=True)` to `cuda()` and replace all mentions of `.data[0]` to `.item()`
3. In `utils.py` and `architect.py` replace `.view(-1)` with `.reshape(-1)`
4. In `architect.py` swap the positions of the parameters in `sub` and `add` calls. 
5. Once the values in step `3` have been swapped add `alpha=` infront of the now second parameter. <br/>
*(Note. example for steps 3-4:* `.add(R, v)` *Becomes* `.add(v, alpha=R)`*)*
6. Create a conda environment with `conda create -n pcdarts python pytorch torchvision cudatookit=11 cudnn -c pytorch` 

### Searching and Training
1. Then search with: `nohup python train_search.py > search.log 2>&1 &`
2. Take the Genotype of the latest epoch from the log file and add it to `genotypes.py` with a name (i.e. `newgeno = Genotype(...)`)
3. Train the search architecture fully with: `nohup python train.py --auxiliary --arch <name of genotype (i.e PCDARTS or newgeno)> > train.log 2>&1 &`
4. Test the final model with: `nohup python test.py --auxiliary --model_path <path to model> > test.log 2>&1 &`
*(Note. I kept gettin state_dict key errors, if * `--auxiliary` *was missing).*

## DrNAS
### Both Darts and 201 Space
1. Create a conda enviroment with Python 3.6
2. Conda install pytorch=1.3.1 torchvision tensorboard cudatoolkit=10 cudnn -c pytorch
3. In the main directory (with the 201-space and DARTS-space directories) run `mkdir experiments`
4. Run `mkdir experiments/cifar10`
5. Run `mkdir experiments/nasbench201`

### Darts Space
1. `cd` into the DARTS-space directory
2. Run the train search algorithm  `nohup python train_search.py --gpu=0 > search.log 2>&1 &`
3. Once completed rename the folder train_search made in the experiments/cifar10 directory to something easy to type such as darts-search.
4. Run the train algortihm `nohup python train.py --model_path=../experiements/cifar10/darts-searcg/weights.pt --gpu=0 > train.log 2>&1 &`
5. In order to get the test error, you need to create a test file. Since it is based off of Darts, I took the test file from Darts and used that.

To just run their cells, simply run: `nohup python train.py > train.log 2>&1 &`

### 201 Space
1. `cd` into the 201-space directory
2. Download the pth from the NAS-Bench-201 [github](https://github.com/D-X-Y/NAS-Bench-201). *(Note. if you are wanting to download the file using wget, follow the instructions at the bottom of this* [page](https://medium.com/@acpanjan/download-google-drive-files-using-wget-3c2c025a8b99)* using '16Y0UwGisiouVRxW-W5hEtbxmcHw_0hF_' as the file id)*
3. In `train_search_progressive.py` and `train_search.py` find `api = API('pth file path')` and change it to point to the pth file you downloaded.
4. pip install nas-bench-201.
5. In `train_search_progressive.py` and `train_search.py`, change the line `result = api.query_by_arch(model.genotype())` to `result = api.query_by_arch(model.genotype(), hp='12')`. *(Note. instead of 12, the hp can also be 200. The hp value must also be a string)*
6. Then run `nohup python train_search.py --gpu=0 > search.log 2>&1 &` using either `train_search.py` or `train_search_progressive.py`
